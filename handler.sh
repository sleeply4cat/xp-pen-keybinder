#!/bin/bash

toppid=$(xdotool getactivewindow getwindowpid)
path=$(ps -p $toppid -o cmd=)
name=$(basename "$path")

cd ./profiles/

if [ ! -f "$name" ]
then
	name="default"
fi

./"$name" $1 $2
